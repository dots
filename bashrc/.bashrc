# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PS1="\W "
export PATH="$PATH:/home/rasmus/.local/scripts"
export EDITOR="vim"


## Aliases
alias ls='ls --color=auto'
alias grep="grep -i --color"
alias c="clear"
alias cal="cal -m"
alias sxiv="sxiv -tb"
alias iv="sxiv -tb ~/pix/wps"
alias update="sudo pacman -Sy archlinux-keyring && sudo pacman -Su"
alias wifi="nmcli connection show"
alias eesti="setxkbmap ee -option caps:ctrl_modifier"
alias usa="setxkbmap us -option caps:ctrl_modifier"
alias vim="nvim"
alias yt="yt-dlp --merge-output-format mkv"




#alias mute="amixer sset Master mute" # amixer sset Master {toggle|+%5}
alias mute="pamixer -t"
alias curVol="pamixer --get-volume-human"
alias micMute='pamixer --source 1 -t'


alias nett="nmcli device wifi list"
# nmcli device wifi connect SSID or BSSID password /password/


# Server stuff
alias server="ssh root@luhamus.com"
alias sesy="rsync -auP ~/server/site/public/ root@luhamus.com:/var/www/site"
alias digisync="rsync -aP --delete --exclude .git/ /home/rasmus/code/digirannak/ root@luhamus.com:/var/www/site/digirannak"



# Pomodoro
declare -A pomo_options
pomo_options["session"]="45"
pomo_options["break"]="10"

pomodoro () {
  if [ -n "$1" -a -n "${pomo_options["$1"]}" ]; then
  val=$1
  echo ${pomo_options["$val"]}m
  echo $val | lolcat
  timer ${pomo_options["$val"]}m
  notify-send $val "Done" -i ~/pix/cowboyBebop.jpg 
  fi
}

alias sess="pomodoro 'session'"
alias brk="pomodoro 'break'"


## While opeing bash
# Stay at the Frequenzy
echo "Each day I add a brick to the wall. Consistency."
