-- Main
vim.opt.nu = true
vim.opt.clipboard = { 'unnamed', 'unnamedplus' }
vim.opt.wrap = false
vim.opt.termguicolors = true
vim.opt.swapfile = false


-- Tabs 
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true


-- Cursor
vim.opt.guicursor = block
vim.opt.scrolloff = 999
vim.opt.sidescrolloff = 999


-- Search
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true



-- Misc
vim.opt.formatoptions:remove{ "c", "r", "o" } -- No nextLine autocomments
vim.opt.smartindent = true
vim.g.netrw_banner = 0



require("miself")
