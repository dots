    local lsp = require('lsp-zero').preset({
  name = 'minimal',
  set_lsp_keymaps = true,
  manage_nvim_cmp = true,
  suggest_lsp_servers = false,
})

-- (Optional) Configure lua language server for neovim
lsp.nvim_workspace()

lsp.ensure_installed = ({
    "quick_lint_js",
    "html-lsp",
    "css-lsp";
    "lua_ls",
    "clangd",
    "python-lsp-server",
})



----------------------
-- Completion stuff --
----------------------

lsp.setup_nvim_cmp({
  preselect = 'none',
  completion = {
    autocomplete = false,
    completeopt = 'menu,menuone,noinsert,noselect',
  },
})



local cmp = require('cmp')
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<C-y>'] = cmp.mapping.confirm( {select = true} ),
    ['<C-Space>'] = cmp.mapping.complete(),
})





lsp.set_preferences({
    sign_icons = { }
})

lsp.setup()




------------------------
-- Disable Diagnostics -
------------------------

vim.diagnostic.config({
  virtual_text = false,
  signs = false,
  update_in_insert = false,
  underline = false,
  severity_sort = false,
  float = false,
})

