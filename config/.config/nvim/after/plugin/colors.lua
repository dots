function ColorItNiceAndGood(color)
    
    ---- Available Coloschemes
    -- rose-pine
    -- catppuccin, catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha

	color = color or "catppuccin"
	vim.cmd.colorscheme(color)
    
    --- Transparency
	--vim.api.nvim_set_hl(0, "Normal", {bg = "none"})
	--vim.api.nvim_set_hl(0, "NormalFloat", {bg = "none"})
end

ColorItNiceAndGood()
