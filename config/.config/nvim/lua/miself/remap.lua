vim.g.mapleader = " "
vim.keymap.set("n", "<leader>e", vim.cmd.Ex)
vim.keymap.set("i", "jj", '<Esc>')


-- Cool Mover
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")


-- Clear Search
vim.keymap.set('n', "<leader>h", ':nohlsearch<CR>')


-- Buffer Tabbing
vim.keymap.set('n', "<S-Tab>", ':e #<CR>')
